package PrimitiveDataTypes;
import java.util.ArrayList;

public class WrapperExample {
    /***
     * Autoboxing là cơ chế tự động chuyển đổi kiểu dữ liệu nguyên thủy sang objec
     *  của wrapper class tương ứng.
     */
    public static void autoboxing(){
        char ch = 'a';
        // Autoboxing - primitive to character object conversion
        Character a = ch;
        System.out.println(a);

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        // Autoboxing because ArrayList stores only object
        arrayList.add(25);
        System.out.println(arrayList.get(0));
    }
    /***
     * Unboxing là cơ chees giúp chuyển đổi các object của wrapper class sang kiểu
     * dữ liệu tương ứng
     */
    public static void unboxing(){
        char ch = 'b';
        // unboxing - primitive to character object conversion
        Character a = ch;
        System.out.println(a);

        ArrayList<Integer> arrayList = new ArrayList<Integer>();      
        arrayList.add(24);
        //unboxing because get mothod returns an Interger object
        int num = arrayList.get(0);
        System.out.println(num);
    }
    public static void main(String[] args) throws Exception {
        WrapperExample.autoboxing();

        WrapperExample.unboxing();
    }
}
