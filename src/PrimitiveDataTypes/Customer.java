package PrimitiveDataTypes;

public class Customer {
    int myNum;// interger (whole number)
    float myFloatNum; //floating point number
    char myLetter; // character
    boolean myBool; //boolean
    String myName; //String

    public Customer(){
        myNum = 5; // interger (whole number)
        myFloatNum = 5.99f;//floating point number
        myLetter = 'H';// character
        myBool = true;//boolean
        myName = "TruongND" ;//String
    }
    public Customer(int num, float floatNum, char letter, boolean bool, String name){
        myNum = num;
        myFloatNum = floatNum;
        myLetter = letter;
        myBool = bool;
        myName = name;
    }
    public static void main(String[] args) throws Exception {
        Customer customer = new Customer();
        System.out.println(customer.myNum);
        System.out.println(customer.myFloatNum);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println(customer.myName);
    
        customer = new Customer(1, 5.88f, 'G', false, "Devcamp");
        System.out.println(customer.myNum);
        System.out.println(customer.myFloatNum);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println(customer.myName);
    }

}

