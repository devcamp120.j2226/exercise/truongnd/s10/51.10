import java.util.Date;
import java.util.Locale;

import PrimitiveDataTypes.Customer;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.text.SimpleDateFormat;

public class App {
  
    public static void main(String[] args) throws Exception {
        LocalDateTime myDateObj = LocalDateTime.now();
        System.out.println("Before formatting: " + myDateObj);
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        String formattedDate = myDateObj.format(myFormatObj);
        System.out.println("After formatting: " + formattedDate);

        App app = new App();
        System.out.println(app.niceDay());
        System.out.println(app.getVietnamDate());
    }
    /**
    * @return String with time format
    */
    public String niceDay(){
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        Date now = new Date();
        return String.format("Have a nice day. It is %s!.", dateFormat.format(now));
    }
    public String getVietnamDate(){
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("EEEE, dd-MMMM-yyyy")
            .localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        return String.format("Hôm nay là %s! Khuyến mãi ngập tràn.", myFormatObj.format(today));
    }

}
